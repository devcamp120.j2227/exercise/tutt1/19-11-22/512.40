// Khai báo thư viện Mongoose
const mongoose = require("mongoose");
const carModel = require("../models/car.model");

const getList = (req, res) => {
    console.log(req.query)
    carModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Loadd all courses successfully!",
                "data": data
            })
        }
    }).skip(+req.query.offset).limit(+req.query.limit);
}

const getDetail = (req, res) => {
    //B1: thu thập dữ liệu
    const carId = req.params.id;

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            "status":"Error 400: bad req",
            "message":"Course Id is not valid!"
        });
    }

    //B3: thực hiện load course theo id
    carModel.findById(carId, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Get course by id successfully!",
                "data":data
            })
        }
    })
}

const create = (req, res) => {
    let bodyCar = req.body;
    console.log(bodyCar);

    //B2: kiểm tra dữ liệu
    // Kiểm tra title
    if (!bodyCar.model || !bodyCar.vId) {
        return res.status(400).json({
            "status": "Error 400: bad req",
            "message": "input is not valid!"
        });
    }

    //B3: thực hiện tạo mới course
    let newCarData = {
        _id: mongoose.Types.ObjectId(),
        model: bodyCar.model,
        vId: bodyCar.vId,
    }

    carModel.create(newCarData, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Create new course successfully!",
                "data":data
            })
        }
    })
}

const update = (req, res) => {
    //B1: thu thập dữ liệu
    const carId = req.params.id;
    let bodyCar = req.body;
    console.log(carId);
    console.log(bodyCar);

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(carId)) {
        return res.status(400).json({
            "status":"Error 400: bad req",
            "message":"Car Id is not valid!"
        });
    }

    //B3: Thực hiện update course theo id
    let updateCarData = {
        model: bodyCar.model,
        vId: bodyCar.vId,
    }
    carModel.findByIdAndUpdate(carId, updateCarData, {returnOriginal: false}, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": error.message,
            })
        } else {
            return res.status(201).json({
                "status":"Update car by id successfully!",
                "data": data,
            })
        }
    })
}

const Delete = (req, res) => {
     //B1: thu thập dữ liệu
     const carId = req.params.id;

     //B2: kiểm tra dữ liệu
     // Kiểm tra course id
     if (!mongoose.Types.ObjectId.isValid(carId)) {
         return res.status(400).json({
             "status":"Error 400: bad request",
             "message":"Car Id is not valid!"
         });
     }
 
 
     //B3: Thực hiện xóa course theo id
     carModel.findByIdAndDelete(carId, (error, data) => {
         if (error) {
             return res.status(500).json({
                 "status":"Error 500: internal server error",
                 "message":error.message
             })
         } else {
             return res.status(201).json({
                 "status":"Delete car by id successfully!",
                 "data":data
             })
         }
     })
}

module.exports = {
    getList,
    getDetail,
    create,
    update,
    Delete,
}