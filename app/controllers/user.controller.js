// Khai báo thư viện Mongoose
const mongoose = require("mongoose");
const userModel = require("../models/user.model");

const getList = (req, res) => {
    console.log(req.query)
    userModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Loadd all user successfully!",
                "data": data
            })
        }
    })
    .skip(+req.query.offset).limit(+req.query.limit)
    .populate('cars');
}

const getDetail = (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.params.id;

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status":"Error 400: bad req",
            "message":"User Id is not valid!"
        });
    }

    //B3: thực hiện load course theo id
    userModel.findById(userId, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Get user by id successfully!",
                "data":data
            })
        }
    })
    .populate('cars');
}

const create = (req, res) => {
    let bodyUser = req.body;
    console.log(bodyUser);

    //B2: kiểm tra dữ liệu
    // Kiểm tra title
    if (!bodyUser.name || !bodyUser.phone) {
        return res.status(400).json({
            "status": "Error 400: bad req",
            "message": "input is not valid!"
        });
    }

    //B3: thực hiện tạo mới course
    let newUserData = {
        _id: mongoose.Types.ObjectId(),
        name: bodyUser.name,
        phone: bodyUser.phone,
        age: bodyUser.age,
        cars: bodyUser.car_ids?.map(car_id => mongoose.Types.ObjectId(car_id)),
    }

    userModel.create(newUserData, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status":"Error 500: internal server error",
                "message":error.message
            })
        } else {
            return res.status(201).json({
                "status":"Create new user successfully!",
                "data":data
            })
        }
    })
}

const update = (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.params.id;
    let bodyUser = req.body;
    console.log(userId);
    console.log(bodyUser);

    //B2: kiểm tra dữ liệu
    // Kiểm tra course id
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status":"Error 400: bad req",
            "message":"User Id is not valid!"
        });
    }

    //B3: Thực hiện update course theo id
    let updateUserData = {
        name: bodyUser.name,
        phone: bodyUser.phone,
        age: bodyUser.age,
        cars: bodyUser.car_ids?.map(car_id => mongoose.Types.ObjectId(car_id)),
    }
    userModel.findByIdAndUpdate(userId, updateUserData, {returnOriginal: false}, (error, data) => {
        if (error) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": error.message,
            })
        } else {
            return res.status(201).json({
                "status":"Update user by id successfully!",
                "data": data,
            })
        }
    })
}

const Delete = (req, res) => {
     //B1: thu thập dữ liệu
     const userId = req.params.id;

     //B2: kiểm tra dữ liệu
     // Kiểm tra course id
     if (!mongoose.Types.ObjectId.isValid(userId)) {
         return res.status(400).json({
             "status":"Error 400: bad request",
             "message":"User Id is not valid!"
         });
     }
 
 
     //B3: Thực hiện xóa course theo id
     userModel.findByIdAndDelete(userId, (error, data) => {
         if (error) {
             return res.status(500).json({
                 "status":"Error 500: internal server error",
                 "message":error.message
             })
         } else {
             return res.status(201).json({
                 "status":"Delete user by id successfully!",
                 "data":data
             })
         }
     })
}

module.exports = {
    getList,
    getDetail,
    create,
    update,
    Delete,
}