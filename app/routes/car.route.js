// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware (if need)
// Import course controller
const carController = require('../controllers/car.controller');

router.get('/', carController.getList);
router.get('/:id', carController.getDetail);
router.post('/', carController.create);
router.put('/:id', carController.update);
router.delete('/:id', carController.Delete);

module.exports = router;