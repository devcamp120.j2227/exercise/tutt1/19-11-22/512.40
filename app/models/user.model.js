// Import thư viện mongoose 
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance reviewSchema từ Class Schema
const userSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
    },
    age: {
        type: Number,
        default: 0,
    },
    cars: [{
        type: mongoose.Types.ObjectId,
        ref: 'Car',
    }]
})

module.exports = mongoose.model("User", userSchema);
