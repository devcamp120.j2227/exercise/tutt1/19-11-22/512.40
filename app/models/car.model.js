// Import thư viện mongoose 
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance reviewSchema từ Class Schema
const carSchema = new Schema({
    model: {
        type: String,
        required: true,
    },
    vId: {
        type: String,
        required: true,
        unique: true,
    },
})

module.exports = mongoose.model("Car", carSchema);