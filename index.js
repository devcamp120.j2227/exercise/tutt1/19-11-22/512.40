// Khai báo thư viên Express
const express = require("express");
var path = require('path');
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo cổng chạy app 
const port = 8000;

// Khai báo để sử dụng body json
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended:true
}))

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_User", (error) => {
    if(error) throw error;

    console.log("Connect successfully!")
})


// Khai báo router app
const carRoute = require('./app/routes/car.route');
const userRoute = require('./app/routes/user.route');

app.use('/cars', carRoute);
app.use('/users', userRoute);

// Chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:", port);
})